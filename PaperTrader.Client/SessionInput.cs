using System.Collections.Generic;
using PaperTrader.Domain;

namespace PaperTrader.Client
{
    public class SessionInput : ISessionInput
    {
        public int Id { get;set;}

        public SessionType SessionType {get;set;}

        public decimal StartBase {get;set;}

        public string HumanReadableName {get;set;}

        public List<string> Tags {get;set;}

        public int Leverage {get;set;}
    }
}