using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PaperTrader.Domain;

namespace PaperTrader.Client
{
    public class PaperTraderConnection : IPaperTraderConnection
    {
        private Uri location;

        public PaperTraderConnection(Uri baseUrl)
        {
            this.location = baseUrl;
        }

        public async Task<Order> CreateOrder(IOrderInput orderInput)
        {
            using (var client = new HttpClient())
            {
                var encodedInput = JsonConvert.SerializeObject(orderInput);
                var httpContent = new StringContent(encodedInput, Encoding.UTF8, "application/json");
                using(var response = await client.PostAsync(location + "/Order", httpContent))
                using (var content = response.Content)
                using (var stream = await content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                {
                    var result = await reader.ReadToEndAsync();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception("Can't create order: " + result);
                    }
                    var sess = JsonConvert.DeserializeObject<Order>(result);
                    return sess;
                }
            }
        }

        public async Task<Session> CreateSession(ISessionInput sessionInput)
        {
            using (var client = new HttpClient())
            {
                var encodedInput = JsonConvert.SerializeObject(sessionInput);
                var httpContent = new StringContent(encodedInput, Encoding.UTF8, "application/json");
                using(var response = await client.PostAsync(location + "/Session", httpContent))
                using(var content = response.Content)
                using (var stream = await content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                {
                    var result = await reader.ReadToEndAsync();
                    Console.WriteLine("response: " + result);
                    var sess = JsonConvert.DeserializeObject<Session>(result);
                    return sess;
                }
            }
        }

        public async Task<Session> GetSession(int sessionId)
        {
            using (var client = new HttpClient())
            {
                using(var response = await client.GetAsync(location + "/Session/" + sessionId))
                using(var content = response.Content)
                using (var stream = await content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                {
                    var result = await reader.ReadToEndAsync();
                    var sess = JsonConvert.DeserializeObject<Session>(result);
                    return sess;
                }
            }
        }

        public async Task<List<Session>> GetSessions()
        {
            using (var client = new HttpClient())
            {
                using(var response = await client.GetAsync(location + "/Session/"))
                using(var content = response.Content)
                using (var stream = await content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                {
                    var result = await reader.ReadToEndAsync();
                    var sess = JsonConvert.DeserializeObject<List<Session>>(result);
                    return sess;
                }
            }
        }

        public async Task<Session> UpdateSession(ISessionInput sessionInput)
        {
            using (var client = new HttpClient())
            {
                var encodedInput = JsonConvert.SerializeObject(sessionInput);
                var httpContent = new StringContent(encodedInput, Encoding.UTF8, "application/json");
                using(var response = await client.PutAsync(location + "/Session", httpContent))
                using(var content = response.Content)
                using (var stream = await content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                {
                    var result = await reader.ReadToEndAsync();
                    var sess = JsonConvert.DeserializeObject<Session>(result);
                    return sess;
                }
            }
        }
    }
}