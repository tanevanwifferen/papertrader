using System;
using System.Threading.Tasks;
using PaperTrader.Domain;

namespace PaperTrader.Client
{
    public class Order
    {
        public int Id {get;set;}

        public int SessionId {get;set;}

        public ISession Session {get;set;}

        public decimal Price {get;set;}

        public decimal SignalPrice {get;set;}

        public int Contracts {get;set;}

        public int ContractsFilled {get;set;}

        public OrderState OrderState {get;set;}

        public DateTime ExecutionTime {get;set;}

        public DateTime SubmissionTime {get;set;}
    }
}