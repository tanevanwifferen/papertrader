using PaperTrader.Domain;

namespace PaperTrader.Client
{
    public class OrderInput : IOrderInput
    {
        public int SessionId {get;set;}

        public decimal Price {get;set;}

        public int Contracts {get;set;}
    }
}