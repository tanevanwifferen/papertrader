using System.Collections.Generic;
using System.Threading.Tasks;
using PaperTrader.Domain;

namespace PaperTrader.Client
{
    public class Session
    {
        public int Id {get;set;}

        public bool CanAddOrders {get;set;}
        public string HumanReadableName {get;set;}
        public int Leverage {get;set;}

        public SessionType SessionType {get;set;}

        public decimal StartBase {get;set;}

        public List<string> Tags { get;set;}
    }
}