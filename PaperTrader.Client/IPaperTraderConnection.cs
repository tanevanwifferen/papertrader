﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaperTrader.Domain;

namespace PaperTrader.Client
{
    interface IPaperTraderConnection
    {
        Task<Session> CreateSession(ISessionInput sessionInput);
        Task<Session> UpdateSession(ISessionInput sessionInput);
        Task<List<Session>> GetSessions();
        Task<Session> GetSession(int sessionId);
        Task<Order> CreateOrder(IOrderInput orderInput);
    }
}
