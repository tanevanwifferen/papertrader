using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto;
using PaperTrader.Web.Dto.Db;
using PaperTrader.Web.Data;
using PaperTrader.Domain;
using System.Collections.Generic;

namespace PaperTrader.Web.Business.Implementations
{
    /// <summary>
    /// SessionManager handles storage logic for sessions.
    /// </summary>
    public class SessionManager : ISessionManager
    {
        /// <summary>
        /// CreateSession creates a new session.
        /// </summary>
        /// <param name="session">Input object of the session.</param>
        /// <returns>A new instance of a Session.</returns>
        public async Task<ISession> CreateSession(ISessionInput session)
        {
            using (var db = new DatabaseContext())
            {
                var dbsession = new DbSession
                {
                    CanAddOrders = true,
                    SessionType = session.SessionType,
                    StartBase = session.StartBase,
                    Leverage = session.Leverage == 0 ? 1 : session.Leverage,
                    HumanReadableName = session.HumanReadableName
                };
                var container = TinyIoC.TinyIoCContainer.Current;
                var tagmgr = container.Resolve<ITagManager>();

                db.DbSessions.Add(dbsession);
                await db.SaveChangesAsync();
                var sess = new Session(dbsession);
                foreach(var tagRaw in session.Tags){
                    var tag = await tagmgr.GetTag(tagRaw);
                    await tagmgr.CreateSessionTag(tag, sess);
                }
                sess.Tags = (await sess.GetTags()).ToList();
                return sess;
            }
        }

        /// <summary>
        /// Sets leverage, if it is a valid value for the BitMEX api.
        /// Throws an ArgumentException if value is invalid.
        /// </summary>
        /// <param name="s">Session object</param>
        /// <param name="leverage">New value for leverage</param>
        public async Task<ISession> UpdateSession(ISession s)
        {
            using (var db = new DatabaseContext())
            {
                var dbsession = await db.DbSessions.FindAsync(s.Id);
                dbsession.Leverage = s.Leverage;
                dbsession.HumanReadableName = s.HumanReadableName;
                dbsession.CanAddOrders = s.CanAddOrders;
                await db.SaveChangesAsync();
                return s;
            }
        }

        /// <summary>
        /// GetOpenSessions gets all open sessions.
        /// </summary>
        /// <returns>A list of all open sessions.</returns>
        public Task<List<ISession>> GetOpenSessions()
        {
            using (var db = new DatabaseContext())
            {
                return db.DbSessions.Where(x => x.CanAddOrders)
                .Select(x => new Session(x))
                .Cast<ISession>().ToListAsync();
            }
        }

        /// <summary>
        /// GetSessionById gets a session, by id, null if not found.
        /// </summary>
        /// <param name="id">Session id to find.</param>
        /// <returns>A session, or null</returns>
        public async Task<ISession> GetSessionById(int id)
        {
            using (var db = new DatabaseContext())
            {
                var dbSession = await db.DbSessions.FirstOrDefaultAsync(x => x.Id == id);
                if (dbSession == null)
                {
                    return null;
                }
                return new Session(dbSession);
            }
        }

        /// <summary>
        /// GetSessions will return all sessions.
        /// </summary>
        /// <returns>A list of all sessions.</returns>
        public Task<List<ISession>> GetSessions()
        {
            using (var db = new DatabaseContext())
            {
                return db.DbSessions.Select(x => new Session(x)).Cast<ISession>().ToListAsync();
            }
        }
    }
}