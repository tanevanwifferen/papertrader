using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Domain;
using PaperTrader.Web.Data;
using PaperTrader.Web.Dto;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using PaperTrader.Domain.Exceptions;
using System.Linq;

namespace PaperTrader.Web.Business.Implementations
{
    /// <summary>
    /// Implementation of IOrdermanager
    /// </summary>
    public class OrderManager : IOrderManager
    {
        /// <summary>
        /// Adds an order to a session if
        /// - enough balance is available
        /// - session is not finalized
        /// 
        /// Throws exception on failure.
        /// 
        /// Accepts a price when the session has type Backtest,
        /// takes current price plus / minus slippage when type
        /// is papertrading.
        /// 
        /// On livetrading, this will also add execution.
        /// </summary>
        /// <param name="o">Order dto</param>
        /// <returns>an awaitable</returns>
        public async Task<IOrder> CreateOrder(
            ISession session,
            decimal price,
            int contracts,
            OrderState state)
        {
            using(var db = new DatabaseContext()){
                Console.WriteLine("session: "+ session);
                Console.WriteLine("price: "+ price);
                Console.WriteLine("contracts: "+ contracts);
                var entity = new Dto.Db.DbOrder(){
                    Contracts = contracts,
                    SignalPrice = price,
                    SubmissionTime = DateTime.Now,
                    SessionId = session.Id,
                    OrderState = state
                };
                db.DbOrders.Add(entity);
                await db.SaveChangesAsync();
                return new Order(entity);
            }
        }

        /// <summary>
        /// Updates an order.
        /// </summary>
        /// <param name="order">New order with values.</param>
        /// <returns>the new version.</returns>
        public async Task<IOrder> UpdateOrder(IOrder order)
        {
            using (var db = new DatabaseContext())
            {
                var dbOrder = db.DbOrders.FirstOrDefault(x => x.Id == order.Id);
                if(dbOrder == null){
                    throw new KeyNotFoundException("Can't find order with id " + order.Id);
                }
                dbOrder.ContractsFilled = order.ContractsFilled;
                dbOrder.OrderState = order.OrderState;
                dbOrder.ExecutionTime = order.ExecutionTime;
                await db.SaveChangesAsync();
                return new Order(dbOrder);
            }
        }

        /// <summary>
        /// Update an order to indicate it was filled, with an x amount, at y price.
        /// </summary>
        /// <param name="order">Order to fill</param>
        /// <param name="amount">Amount to fill</param>
        /// <param name="price">Price to fill at</param>
        /// <returns>An awaitable</returns>
        public async Task FillOrder(IOrder order, int amount, decimal price, DateTime executionTime)
        {
            using(var db = new DatabaseContext()){
                var entity = db.DbOrders.Find(order.Id);
                entity.ContractsFilled += amount;
                entity.Price = price;
                entity.ExecutionTime = executionTime;
                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// GetOrdersForSession gets all orders for a specific session.
        /// </summary>
        /// <param name="s">Session to fetch orders for</param>
        /// <returns>Orders for given session</returns>
        public Task<List<IOrder>> GetOrdersForSession(ISession s)
        {
            using(var db = new DatabaseContext()){
                return db.DbOrders.Where(x => x.SessionId == s.Id)
                .Select(x => new Order(x))
                .Cast<IOrder>().ToListAsync();
            }
        }
    }
}