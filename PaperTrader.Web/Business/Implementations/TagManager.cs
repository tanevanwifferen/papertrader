using System.Threading.Tasks;
using System;
using PaperTrader.Domain;
using PaperTrader.Web.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PaperTrader.Web.Dto.Db;
using PaperTrader.Web.Dto;
using PaperTrader.Web.Business.Interfaces;
using System.Collections.Generic;

namespace PaperTrader.Web.Business.Implementations
{
    public class TagManager : ITagManager
    {
        public async Task<ITag> GetTag(string value){
            using(var db = new DatabaseContext()){
                var existing = await db.DbTags.FirstOrDefaultAsync(x => x.Value == value);
                if(existing == null){
                    existing = new DbTag
                    {
                        Value = value
                    };
                    db.DbTags.Add(existing);
                    await db.SaveChangesAsync();
                }
                return new Tag(existing);
            }
        }

        public Task<List<ITag>> GetAll(){
            using (var db = new DatabaseContext())
            {
                return db.DbTags.Select(x => new Tag(x))
                .Cast<ITag>().ToListAsync();
            }
        }

        public async Task<ITag> GetTag(int id){
            using(var db = new DatabaseContext()){
                var dbTag = await db.DbTags.FirstAsync(x => x.Id == id);
                var tag = new Tag(dbTag);
                return tag;
            }
        }

        public async Task RemoveSessionTag(ISessionTag toRemove){
            using (var db = new DatabaseContext())
            {
                var existing = await db.DbSessionTags
                .FirstOrDefaultAsync(x => x.SessionId == toRemove.SessionId && x.TagId == toRemove.TagId);
                if(existing == null){
                    return;
                }
                db.DbSessionTags.Remove(existing);
                await db.SaveChangesAsync();
            }
        }

        public async Task<DbSessionTag> CreateTag(string value, ISession session){
            using(var db = new DatabaseContext()){
                var tag = this.GetTag(value);
                var sessionTag = await db.DbSessionTags
                .FirstOrDefaultAsync(x => x.TagId == tag.Id && x.SessionId == session.Id);
                if(sessionTag == null){
                    sessionTag = new DbSessionTag{
                        TagId = tag.Id,
                        SessionId = session.Id
                    };
                    db.DbSessionTags.Add(sessionTag);
                    await db.SaveChangesAsync();
                }
                return sessionTag;
            }
        }

        public async Task RemoveSessionTag(DbSessionTag t){
            using(var db = new DatabaseContext()){
                if(t.Id == 0){
                    throw new InvalidOperationException("invalid id for removal of tag");
                }
                db.DbSessionTags.Remove(t);
                await db.SaveChangesAsync();
            }
        }

        public async Task RemoveTagsForSession(ISession session){
            using(var db = new DatabaseContext()){
                var sessionTags =db.DbSessionTags.Where(x =>x.SessionId == session.Id);
                db.DbSessionTags.RemoveRange(sessionTags);
                await db.SaveChangesAsync();
            }
        }

        public async Task CreateSessionTag(string value, ISession session){
            using(var db = new DatabaseContext()){
                var tag = await this.GetTag(value);
                await CreateSessionTag(tag, session);
            }
        }

        public async Task CreateSessionTag(ITag tag, ISession session){
            using (var db = new DatabaseContext())
            {
                var existing = await db.DbSessionTags
                .FirstOrDefaultAsync(x => x.SessionId == session.Id && x.TagId == tag.Id);
                if(existing != null){
                    return;
                }

                var sessionTag = new DbSessionTag
                {
                    TagId = tag.Id,
                    SessionId = session.Id
                };
                db.DbSessionTags.Add(sessionTag);
                await db.SaveChangesAsync();
            }
        }

        public async Task<List<ITag>> GetForSession(ISession s){
            using(var db = new DatabaseContext()){
                var sessionTags = db.DbSessionTags
                .Where(x => x.SessionId == s.Id);
                var toReturn = new List<ITag>();
                foreach(var st in sessionTags){
                    toReturn.Add((ITag)await this.GetTag(st.TagId));
                }
                return toReturn;
            }
        }
    }
}