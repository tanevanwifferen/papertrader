using System.Collections.Generic;
using System.Threading.Tasks;
using PaperTrader.Domain;
using PaperTrader.Web.Dto.Db;

namespace PaperTrader.Web.Business.Interfaces
{
    public interface ITagManager
    {
        Task<ITag> GetTag(string value);
        Task<ITag> GetTag(int id);
        Task<List<ITag>> GetAll();
        Task<List<ITag>> GetForSession(ISession s);
        Task RemoveSessionTag(ISessionTag toRemove);
        Task CreateSessionTag(ITag tag, ISession session);
        Task RemoveTagsForSession(ISession session);
        Task CreateSessionTag(string value, ISession session);
    }
}