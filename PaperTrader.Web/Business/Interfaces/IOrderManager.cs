using PaperTrader.Domain;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace PaperTrader.Web.Business.Interfaces
{
    /// <summary>
    /// Interface to do order backend logic.
    /// </summary>
    public interface IOrderManager
    {
        /// <summary>
        /// Adds an order to a session if
        /// - enough balance is available
        /// - session is not finalized
        /// 
        /// Throws exception on failure.
        /// 
        /// Accepts a price when the session has type Backtest,
        /// takes current price plus / minus slippage when type
        /// is papertrading.
        /// 
        /// On livetrading, this will also add execution.
        /// </summary>
        /// <param name="order"></param>
        /// <returns>The new order</returns>
        Task<IOrder> CreateOrder(ISession session, decimal Price, int Contracts, OrderState state);

         /// <summary>
         /// Update an order to indicate it was filled, with an x amount, at y price.
         /// </summary>
         /// <param name="order">Order to fill</param>
         /// <param name="amount">Amount to fill</param>
         /// <param name="price">Price to fill at</param>
         /// <param name="executionTime">time of execution</param>
         /// <returns>An awaitable.</returns>
        Task FillOrder(IOrder order, int amount, decimal price, DateTime executionTime);

         /// <summary>
         /// GetOrdersForSession returns all orders belonging to a session.
         /// </summary>
         /// <param name="s">Session to get orders for</param>
         /// <returns>The orders for this session.</returns>
        Task<List<IOrder>> GetOrdersForSession(ISession s);
    }
}