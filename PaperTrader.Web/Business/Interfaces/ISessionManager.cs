using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using PaperTrader.Domain;

namespace PaperTrader.Web.Business.Interfaces
{
    /// <summary>
    /// ISessionManager is a middleware interface that creates, lists, modifies
    /// sessions.
    /// </summary>
    public interface ISessionManager
    {
         /// <summary>
         /// GetSessions gets all sessions currently in storage.
         /// </summary>
         /// <returns>All sessions currently in storage</returns>
        Task<List<ISession>> GetSessions();

        /// <summary>
        /// GetOpenSessions gets all currently open sessions.
        /// </summary>
        /// <returns>A list of all currently open sessions.</returns>
        Task<List<ISession>> GetOpenSessions();

        /// <summary>
        /// GetSessionById returns a session with given id, or null if  
        /// none is found.
        /// </summary>
        /// <param name="id">Id of the session to fetch</param>
        /// <returns>The session for id, or null if none found with that id.</returns>
        Task<ISession> GetSessionById(int id);

        /// <summary>
        /// Sets leverage, if it is a valid value for the BitMEX api.
        /// Throws an ArgumentException if value is invalid.
        /// </summary>
        /// <param name="s">Session object</param>
        /// <param name="leverage">New value for leverage</param>
        Task<ISession> UpdateSession(ISession s);

        /// <summary>
        /// Createsession creates a new session.
        /// </summary>
        /// <param name="session">Input for this session.</param>
        /// <returns>A new session instance.</returns>
        Task<ISession> CreateSession(ISessionInput session);
    }
}