﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PaperTrader.Web.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbSessions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HumanReadableName = table.Column<string>(nullable: true),
                    Leverage = table.Column<int>(nullable: false),
                    CanAddOrders = table.Column<bool>(nullable: false),
                    SessionType = table.Column<int>(nullable: false),
                    StartBase = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SessionId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    SignalPrice = table.Column<decimal>(nullable: false),
                    Contracts = table.Column<int>(nullable: false),
                    ContractsFilled = table.Column<int>(nullable: false),
                    ExecutionTime = table.Column<DateTime>(nullable: false),
                    SubmissionTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbOrders_DbSessions_SessionId",
                        column: x => x.SessionId,
                        principalTable: "DbSessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DbSessionTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SessionId = table.Column<int>(nullable: false),
                    DbSessionId = table.Column<int>(nullable: true),
                    TagId = table.Column<int>(nullable: false),
                    DbTagId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSessionTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbSessionTags_DbSessions_DbSessionId",
                        column: x => x.DbSessionId,
                        principalTable: "DbSessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DbSessionTags_DbTags_DbTagId",
                        column: x => x.DbTagId,
                        principalTable: "DbTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbOrders_SessionId",
                table: "DbOrders",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_DbSessionTags_DbSessionId",
                table: "DbSessionTags",
                column: "DbSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_DbSessionTags_DbTagId",
                table: "DbSessionTags",
                column: "DbTagId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbOrders");

            migrationBuilder.DropTable(
                name: "DbSessionTags");

            migrationBuilder.DropTable(
                name: "DbSessions");

            migrationBuilder.DropTable(
                name: "DbTags");
        }
    }
}
