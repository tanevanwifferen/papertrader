﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PaperTrader.Web.Data;

namespace PaperTrader.Web.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20181220191741_addorderstate")]
    partial class addorderstate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Contracts");

                    b.Property<int>("ContractsFilled");

                    b.Property<DateTime>("ExecutionTime");

                    b.Property<int>("OrderState");

                    b.Property<decimal>("Price");

                    b.Property<int>("SessionId");

                    b.Property<decimal>("SignalPrice");

                    b.Property<DateTime>("SubmissionTime");

                    b.HasKey("Id");

                    b.HasIndex("SessionId");

                    b.ToTable("DbOrders");
                });

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbSession", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("CanAddOrders");

                    b.Property<string>("HumanReadableName");

                    b.Property<int>("Leverage");

                    b.Property<int>("SessionType");

                    b.Property<decimal>("StartBase");

                    b.HasKey("Id");

                    b.ToTable("DbSessions");
                });

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbSessionTag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DbSessionId");

                    b.Property<int?>("DbTagId");

                    b.Property<int>("SessionId");

                    b.Property<int>("TagId");

                    b.HasKey("Id");

                    b.HasIndex("DbSessionId");

                    b.HasIndex("DbTagId");

                    b.ToTable("DbSessionTags");
                });

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbTag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("DbTags");
                });

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbOrder", b =>
                {
                    b.HasOne("PaperTrader.Web.Dto.Db.DbSession", "Session")
                        .WithMany("Orders")
                        .HasForeignKey("SessionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperTrader.Web.Dto.Db.DbSessionTag", b =>
                {
                    b.HasOne("PaperTrader.Web.Dto.Db.DbSession")
                        .WithMany("DbSessionTags")
                        .HasForeignKey("DbSessionId");

                    b.HasOne("PaperTrader.Web.Dto.Db.DbTag")
                        .WithMany("DbSessions")
                        .HasForeignKey("DbTagId");
                });
#pragma warning restore 612, 618
        }
    }
}
