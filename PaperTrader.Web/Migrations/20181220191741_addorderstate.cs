﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PaperTrader.Web.Migrations
{
    public partial class addorderstate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderState",
                table: "DbOrders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderState",
                table: "DbOrders");
        }
    }
}
