using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;

namespace PaperTrader.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TagController : ControllerBase
    {
        [HttpGet]
        public Task<List<ITag>> GetAllAsync(){
            var tagMgr = TinyIoC.TinyIoCContainer.Current.Resolve<ITagManager>();
            return tagMgr.GetAll();
        }
    }
}