using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaperTrader.Domain;
using PaperTrader.Domain.Exceptions;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto;

namespace PaperTrader.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        [HttpGet]
        [Route("GetForSession/{sessionId:int}")]
        public async Task<ActionResult<List<IOrder>>> GetForSessionAsync(int sessionId)
        {
            var container =TinyIoC.TinyIoCContainer.Current; 
            var ordersController = container.Resolve<IOrderManager>();
            var sessionsController = container.Resolve<ISessionManager>();
            var session = await sessionsController.GetSessionById(sessionId);
            return await ordersController.GetOrdersForSession(session);
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrder(OrderInput orderInput){
            var container = TinyIoC.TinyIoCContainer.Current;
            var ordersController = container.Resolve<IOrderManager>();
            var sessionController = container.Resolve<ISessionManager>();
            var sess = await sessionController.GetSessionById(orderInput.SessionId);
            if(sess == null){
                return this.NotFound();
            }
            try{
                var order = await sess.AddOrder(orderInput.Price, orderInput.Contracts);
                return new JsonResult(order);
            } catch (BalanceInsufficientException e){
                return this.Conflict(new {Message=e.Message});
            }
        }
    }
}