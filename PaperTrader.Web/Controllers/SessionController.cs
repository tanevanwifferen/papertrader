using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto;

namespace PaperTrader.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SessionController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<List<ISession>>> GetAllAsync()
        {
            var sessionController = TinyIoC.TinyIoCContainer.Current.Resolve<ISessionManager>();
            var sessions = await sessionController.GetSessions();
            foreach(var s in sessions){
                s.Tags =  (await s.GetTags()).ToList();
            }
            return sessions;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ISession>> GetAsync(int id)
        {
            if(id == 0){
                return this.BadRequest("id must be given");
            }
            var sessionController = TinyIoC.TinyIoCContainer.Current.Resolve<ISessionManager>();
            var session = await sessionController.GetSessionById(id);
            if(session == null){
                return this.NotFound("can't find session with id " + id);
            }
            session.Tags = (await session.GetTags()).ToList();
            return new JsonResult(session);
        }

        [HttpPost]
        public async Task<ActionResult<ISession>> PostAsync([FromBody]SessionInput sessionInput){
            var sessionMgr = TinyIoC.TinyIoCContainer.Current.Resolve<ISessionManager>();
            var toReturn = await sessionMgr.CreateSession(sessionInput);
            return new JsonResult(toReturn);
        }

        [HttpPut]
        public async Task<ActionResult<ISession>> PutAsync([FromBody]SessionInput sessionInput){
            if(sessionInput.Id == 0){
                return this.BadRequest("no id given");
            }
            var container = TinyIoC.TinyIoCContainer.Current;
            var sessionmgr = container.Resolve<ISessionManager>();
            var tagmgr = container.Resolve<ITagManager>();
            var session = await sessionmgr.GetSessionById(sessionInput.Id);
            session.Leverage = sessionInput.Leverage;
            session.HumanReadableName = sessionInput.HumanReadableName;
            await tagmgr.RemoveTagsForSession(session);
            foreach(var el in sessionInput.Tags){
                await tagmgr.CreateSessionTag(el, session);
            }
            var toReturn = await sessionmgr.UpdateSession(session);
            return new JsonResult(toReturn);
        }
    }
}