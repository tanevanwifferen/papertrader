using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto;

namespace PaperTrader.Web.Pages.Sessions
{
    public class SessionDetailsModel : PageModel
    {
        public ISession Session{get;set;}

        [BindProperty]
        public OrderInput Order{get;set;} = new OrderInput();

        [BindProperty]
        public string HumanReadableName{get;set;}

        [BindProperty]
        public int SessionId{get;set;}

        [BindProperty]
        public string Action{get;set;}

        public async Task<ActionResult> OnGetAsync(int id)
        {
            if(id == default(int)){
                return NotFound();
            }
            var container = TinyIoC.TinyIoCContainer.Current;
            var sessionmanager = container.Resolve<ISessionManager>();
            var sess = await sessionmanager.GetSessionById(id);
            if(sess == null){
                return NotFound();
            }
            this.Session = sess;
            this.SessionId = sess.Id;
            return Page();
        }
    }
}