using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto;

namespace PaperTrader.Web.Pages.Sessions
{
    public class CreateSessionModel : PageModel
    {
        [BindProperty]
        public SessionInput Input{get;set;}
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(){
            var container = TinyIoC.TinyIoCContainer.Current;
            await container.Resolve<ISessionManager>().CreateSession(this.Input);
            return Redirect("/Sessions/SessionList");
        }
    }
}