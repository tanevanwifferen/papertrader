using Microsoft.EntityFrameworkCore;
using PaperTrader.Web.Dto.Db;

namespace PaperTrader.Web.Data
{
    /// <summary>
    /// Storage class for entity framework.
    /// </summary>
    public class DatabaseContext:DbContext
    {
        /// <summary>
        /// Gets or sets a dbset of orders.
        /// </summary>
        public DbSet<DbOrder> DbOrders{get;set;}
        /// <summary>
        /// Gets or sets a dbset of sessions.
        /// </summary>
        public DbSet<DbSession> DbSessions{get;set;}

        /// <summary>
        /// Gets or sets a dbset holding all tags for this session.
        /// </summary>
        public DbSet<DbSessionTag> DbSessionTags{get;set;}

        /// <summary>
        /// Gets or sets a dbset of tags.
        /// </summary>
        public DbSet<DbTag> DbTags{get;set;}

        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {


            optionsBuilder.UseSqlite("Data Source=Sessions.db");
        }
    }
}