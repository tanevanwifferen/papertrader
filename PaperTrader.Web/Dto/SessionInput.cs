using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;

namespace PaperTrader.Web.Dto
{
    public class SessionInput : ISessionInput
    {
        public int Id{get;set;}

        [Required]
        public SessionType SessionType { get; set; }

        [Required]
        public decimal StartBase { get; set; }

        public string HumanReadableName { get; set; }
        public List<string> Tags{get;set;}
        public int Leverage{get;set;}
    }
}