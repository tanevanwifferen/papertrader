using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;
using PaperTrader.Web.Dto.Db;

namespace PaperTrader.Web.Dto
{
    public class Tag : ITag
    {
        private DbTag _dbTag;

        public Tag(DbTag dbTag){
            this._dbTag = dbTag;
        }
        public int Id => this._dbTag.Id;

        public string Value => this._dbTag.Value;

        public Task<ICollection<ISession>> GetSessions() {
            var container = TinyIoC.TinyIoCContainer.Current;
            var sessionMgr = container.Resolve<ISessionManager>();
            return Task.Run(async ()=>{
                var toReturn = new List<ISession>();
                foreach (var sessionTag in this._dbTag.DbSessions)
                {
                    toReturn.Add(await sessionMgr.GetSessionById(sessionTag.SessionId));
                }
                return (ICollection<ISession>) toReturn;
            });
        }
    }
}