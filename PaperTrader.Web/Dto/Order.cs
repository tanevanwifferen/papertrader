using PaperTrader.Web.Dto.Db;
using PaperTrader.Domain;
using System;
using PaperTrader.Web.Business.Interfaces;
using System.Threading.Tasks;

namespace PaperTrader.Web.Dto
{
    /// <summary>
    /// Encapsulation class for DbOrder, business object.
    /// </summary>
    public class Order: PaperTrader.Domain.IOrder
    {
        private DbOrder _order;
        /// <summary>
        /// Initializes a new <see cref="Order" /> object.
        /// </summary>
        /// <param name="order">DbOrder to use as base object.</param>
        public Order(DbOrder order){
            this._order = order;
        }

        /// <summary>
        /// Gets the order id
        /// </summary>
        public int Id => _order.Id;

        public OrderState OrderState => _order.OrderState;

        /// <summary>
        /// Gets the session id
        /// </summary>
        public int SessionId => _order.SessionId;

        /// <summary>
        /// Gets the session.
        /// </summary>
        /// <returns>The session belonging to this order.</returns>
        public virtual ISession Session => (ISession) _order.Session;

        /// <summary>
        /// Gets the price this order was filled at.
        /// </summary>
        public decimal Price=> _order.Price;

        /// <summary>
        /// Gets the price this order was added at.
        /// </summary>
        public decimal SignalPrice => _order.SignalPrice;

        /// <summary>
        /// Gets the amount of contracts for this order.
        /// </summary>
        public int Contracts => _order.Contracts;

        /// <summary>
        /// Gets the amount that is currently filled for this order.
        /// </summary>
        public int ContractsFilled => _order.ContractsFilled;

        /// <summary>
        /// Gets the time this order was executed.
        /// </summary>
        public DateTime ExecutionTime => _order.ExecutionTime;

        /// <summary>
        /// Gets the time this order was submitted.
        /// </summary>
        public DateTime SubmissionTime => _order.SubmissionTime;

        /// <summary>
        /// Fill this order with an amount at a price.
        /// </summary>
        /// <param name="executionTime">Time of execution</param>
        /// <param name="amount">Amount to execute</param>
        /// <param name="price">Price to execute at.</param>
        public Task Fill(DateTime executionTime, int amount, decimal price){
            if(this.ContractsFilled == this.Contracts){
                throw new InvalidOperationException("can't fill more than once");
            }
            if(Math.Abs(this.ContractsFilled + amount) > Math.Abs(this.Contracts)){
                throw new InvalidOperationException("Can't fill with more contracts than ordered");
            }
            var container = TinyIoC.TinyIoCContainer.Current;
            return container.Resolve<IOrderManager>().FillOrder(this, amount, price, executionTime);
        }
    }
}