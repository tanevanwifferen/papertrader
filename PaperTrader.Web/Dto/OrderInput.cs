using PaperTrader.Domain;
using PaperTrader.Web.Business.Interfaces;

namespace PaperTrader.Web.Dto
{
    public class OrderInput : IOrderInput
    {
        public int SessionId {get;set;}

        public decimal Price {get;set;}

        public int Contracts {get;set;}
    }
}