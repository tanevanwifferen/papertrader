using System.Collections.Generic;

namespace PaperTrader.Web.Dto.Db
{
    public class DbTag
    {
        public DbTag(){
            this.DbSessions = new HashSet<DbSessionTag>();
        }
        public int Id{get;set;}
        public string Value{get;set;}
        public virtual ICollection<DbSessionTag> DbSessions{get;set;}
    }
}