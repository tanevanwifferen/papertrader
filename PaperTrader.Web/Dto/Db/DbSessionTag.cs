using PaperTrader.Domain;

namespace PaperTrader.Web.Dto.Db
{
    public class DbSessionTag : ISessionTag
    {
        public int Id { get; set; }
        public int SessionId { get; set; }

        public int TagId { get; set; }
    }
}