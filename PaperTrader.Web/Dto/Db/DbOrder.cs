using System;
using System.ComponentModel.DataAnnotations;
using PaperTrader.Domain;

namespace PaperTrader.Web.Dto.Db
{
    public class DbOrder
    {
        public int Id{get;set;}
        public int SessionId{get;set;}
        public virtual DbSession Session {get;set;}
        public decimal Price{get;set;}
        public decimal SignalPrice{get;set;}
        public OrderState OrderState{get;set;}
        public int Contracts{get;set;}
        public int ContractsFilled{get;set;}
        public DateTime ExecutionTime{get;set;}
        public DateTime SubmissionTime{get;set;}
    }
}