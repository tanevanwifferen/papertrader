using PaperTrader.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PaperTrader.Web.Dto.Db
{
    /// <summary>
    /// Session dto used in database.
    /// </summary>
    public class DbSession
    {
        public DbSession(){
            this.DbSessionTags = new HashSet<DbSessionTag>();
        }
        /// <summary>
        /// Gets or sets the id of the DbSession.
        /// </summary>
        public int Id{get;set;}

        /// <summary>
        /// Gets or sets the human readable name for this session.
        /// </summary>
        public string HumanReadableName{get;set;}

        /// <summary>
        /// Gets or sets the leverage used in this session.
        /// </summary>
        public int Leverage{get;set;}

        /// <summary>
        /// Gets or sets a value indicating whether this session can 
        /// have new orders added.
        /// </summary>
        public bool CanAddOrders{get;set;}

        /// <summary>
        /// Gets or sets the session type of this session.
        /// </summary>
        public SessionType SessionType{get;set;}
        
        /// <summary>
        /// Gets or sets the start base currency of this session.
        /// </summary>
        public decimal StartBase{get;set;}

        /// <summary>
        /// Gets or sets the orders for this DbSession
        /// </summary>
        public virtual List<DbOrder> Orders { get; set; }

        /// <summary>
        /// Gets or sets the tags for this DbSession
        /// </summary>
        public virtual ICollection<DbSessionTag> DbSessionTags {get;set;}
    }
}