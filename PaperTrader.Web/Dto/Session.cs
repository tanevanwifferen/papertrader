using PaperTrader.Web.Dto.Db;
using PaperTrader.Domain;
using System;
using System.Collections.Generic;
using PaperTrader.Domain.Exceptions;
using TinyIoC;
using PaperTrader.Web.Business.Interfaces;
using System.Threading.Tasks;
using System.Linq;

namespace PaperTrader.Web.Dto
{
    /// <summary>
    /// Encapsulation class for dbsession, business object.
    /// </summary>
    public class Session : ISession
    {
        private DbSession _session;

        /// <summary>
        /// Initializes a new instance of the Session class.
        /// </summary>
        public Session(Db.DbSession session)
        {
            this._session = session;
        }

        /// <summary>
        /// Gets the id of this session;
        /// </summary>
        public int Id => _session.Id;

        /// <summary>
        /// Gets the human readable name for this session.
        /// </summary>
        public string HumanReadableName
        {
            get { return _session.HumanReadableName; }
            set { _session.HumanReadableName = value; }
        }

        /// <summary>
        /// Gets the leverage used in this session.
        /// </summary>
        public int Leverage
        {
            get { return _session.Leverage; }
            set
            {
                var allowedLeverages = new[] { 1, 2, 3, 5, 10, 25, 50, 100 };
                if (!allowedLeverages.Any(x => x == value))
                {
                    throw new ArgumentException("Invalid leverage provided");
                }
                _session.Leverage = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the session can have orders added.
        /// </summary>
        public bool CanAddOrders
        {
            get { return this._session.CanAddOrders; }
            set
            {
                if (!this._session.CanAddOrders)
                {
                    throw new InvalidOperationException("session already closed");
                }
                this._session.CanAddOrders = value;
            }
        }

        /// <summary>
        /// Gets the sessiontype of this session.
        /// </summary>
        public SessionType SessionType => _session.SessionType;

        /// <summary>
        /// Gets the starting base amount for this session.
        /// </summary>
        public decimal StartBase => _session.StartBase;

        /// <summary>
        /// Gets the set of tasks for this Session.
        /// </summary>
        public List<string> Tags {get;set;}

        /// <summary>
        /// Gets the tags for this session.
        /// </summary>
        public async Task<ICollection<string>> GetTags(){
            var container = TinyIoC.TinyIoCContainer.Current;
            var tagMgr = container.Resolve<ITagManager>();
            var tags = await tagMgr.GetForSession(this);
            return tags.Select(x => x.Value).ToList();
        }

        /// <summary>
        /// Gets a list of all orders for this Session.
        /// </summary>
        public Task<List<IOrder>> GetOrders(){
            var container = TinyIoC.TinyIoCContainer.Current;
            var orderManager = container.Resolve<IOrderManager>();
            return orderManager.GetOrdersForSession(this);
        }

        /// <summary>
        /// Add an order to this session.
        /// </summary>
        /// <param name="order">Order to add</param>
        public async Task<IOrder> AddOrder(decimal price, int contracts)
        {
            if (!this.CanAddOrders)
            {
                throw new InvalidOperationException("Can't add order, session " + this.Id + " is closed");
            }
            // positive for long pos, negative for short
            var currentContracts = await this.FinalContracts() / this.Leverage;
            OrderState state = OrderState.Filled;
            switch(this.SessionType){
                case SessionType.Backtest:
                    break;
                default:
                throw new NotImplementedException("SessionType " + this.SessionType.ToString() + " not implemented yet.");
            }

            if (currentContracts < 0 && contracts < 0 || currentContracts > 0 && contracts > 0)
            {
                // we are growing our position
                var availableMargin = await this.FinalAvailableBalance();
                var marginNeededBtc = contracts / price;
                if (marginNeededBtc > availableMargin)
                {
                    throw new BalanceInsufficientException();
                }
            }

            var container = TinyIoCContainer.Current;
            var orderManager = container.Resolve<IOrderManager>();
            return await orderManager.CreateOrder(this, price, contracts, state);
        }

        /// <summary>
        /// SaveChanges saves the changes to persistent storage.
        /// </summary>
        /// <returns>An awaitable</returns>
        public async Task SaveChanges()
        {
            var container = TinyIoCContainer.Current;
            var sessionManager = container.Resolve<ISessionManager>();
            await sessionManager.UpdateSession(this);
        }

        /// <summary>
        /// Closes the session.
        /// </summary>
        /// <returns>an awaitable</returns>
        public Task Close()
        {
            this.CanAddOrders = false;
            return this.SaveChanges();
        }

        /// <summary>
        /// FinalMarginBalance() gets the current total margin amount
        /// </summary>
        /// <returns>The margin balance when all orders have been executed.</returns>
        public async Task<decimal> FinalMarginBalance()
        {
            var orders = await this.GetOrders();
            var baseMargin = this.StartBase;
            var contracts = 0;
            var latestPrice = 0m;
            foreach (var order in orders)
            {
                var p = order.Price == 0 ? order.SignalPrice : order.Price;
                var price = order.Contracts / p;

                baseMargin += price;
                contracts = contracts + order.Contracts;
                latestPrice = p;
            }
            if (latestPrice == 0)
            {
                return this.StartBase;
            }
            return (contracts / latestPrice) + baseMargin;
        }

        /// <summary>
        /// FinalAvailableBalance() gets the amount of available margin
        /// </summary>
        /// <returns>The balance that is left when all trades have been executed.</returns>
        public async Task<decimal> FinalAvailableBalance()
        {
            var orders = await this.GetOrders();
            var baseMargin = this.StartBase;
            var latestPrice = 0m;
            var contracts = 0m;
            foreach (var order in orders)
            {
                var price = order.Contracts / order.SignalPrice;
                if (contracts * order.Contracts > 0)
                {
                    // margin usage increases, base decreases
                    baseMargin -= Math.Abs(price);
                }
                else
                {
                    // margin usage decreases, base increases
                    baseMargin += Math.Abs(price);
                }
                contracts += order.Contracts;
            }
            if (latestPrice == 0)
            {
                return this.StartBase;
            }
            return baseMargin;
        }

        /// <summary>
        /// FinalContracts() gets the amount of contracts currently held
        /// </summary>
        /// <returns>The amount of contracts held after all trades have been executed.</returns>
        public async Task<int> FinalContracts()
        {
            var orders = await this.GetOrders();
            var baseMargin = this.StartBase;
            var contracts = 0;
            foreach (var order in orders)
            {
                contracts += order.Contracts;
            }
            return contracts;
        }
    }
}