using System;
using PaperTrader.Web.Data;
using PaperTrader.Web.Dto.Db;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace PaperTrader.Tests
{
    public class DbContextTests
    {
        [Fact]
        public void TestDbContextExists()
        {
            using(var context = new DatabaseContext()){
                context.Orders.Add(new DbOrder{
                    Amount = 1
                });
                context.SaveChanges();
            }
            using(var context = new DatabaseContext()){
                Assert.NotEmpty(context.Orders);
            }
        }
    }
}
