﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaperTrader.Domain
{
    /// <summary>
    ///  A session is a set of trades, for a specific backtest/trading session.
    ///  This allows multiple backtests / paper trading sessions at once, either live or 
    ///  with historic data.
    /// </summary>
    public interface ISession
    {
        /// <summary>
        /// Identifier for the session
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Gets a value indicating if any new orders can be created on the Session.
        /// </summary>
        bool CanAddOrders { get; set; }

        /// <summary>
        /// Gets a human readable value to identify the session.
        /// </summary>
        string HumanReadableName { get; set; }

        /// <summary>
        /// Gets the leverage used in this session.
        /// </summary>
        int Leverage { get; set; }

        /// <summary>
        /// Gets a value indicating which type is in use in this Session
        /// </summary>
        SessionType SessionType { get; }

        /// <summary>
        /// StartBase is the amount of BTC on the account at initialization.
        /// </summary>
        decimal StartBase { get; }

        /// <summary>
        /// Tags gets the set of tags for this ISession. 
        /// Does not need to be filled.
        /// </summary>
        List<string> Tags{get;set;}

        /// <summary>
        /// Gets the tags for this session.
        /// </summary>
        Task<ICollection<string>> GetTags();

        /// <summary>
        /// SaveChanges persistently saves the changes to the object.
        /// </summary>
        /// <returns>an awaitable.</returns>
        Task SaveChanges();

        /// <summary>
        /// Closes the session, and saves it too
        /// </summary>
        /// <returns>an awaitable</returns>
        Task Close();

        /// <summary>
        /// AddOrder adds an order to this Session
        /// </summary>
        /// <param name="orderInput">OrderInput to add</param>
        /// <returns>An awaitable.</returns>
        Task<IOrder> AddOrder(decimal price, int contracts);

        /// <summary>
        /// FinalMarginBalance() gets the current total margin amount
        /// </summary>
        /// <returns>The margin balance when all orders have been executed.</returns>
        Task<decimal> FinalMarginBalance();

        /// <summary>
        /// FinalAvailableBalance() gets the amount of available margin
        /// </summary>
        /// <returns>The balance that is left when all trades have been executed.</returns>
        Task<decimal> FinalAvailableBalance();

        /// <summary>
        /// FinalContracts() gets the amount of contracts currently held
        /// </summary>
        /// <returns>The amount of contracts held after all trades have been executed.</returns>
        Task<int> FinalContracts();

        /// <summary>
        /// Gets all orders for this Session
        /// </summary>
        /// <returns>A list of orders.</returns>
        Task<List<IOrder>> GetOrders();
    }
}
