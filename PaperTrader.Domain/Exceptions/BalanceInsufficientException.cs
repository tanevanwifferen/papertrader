namespace PaperTrader.Domain.Exceptions
{
    /// <summary>
    /// BalanceInsufficientException is thrown when an order 
    /// can't be executed because of margin not being enough.
    /// </summary>
    public class BalanceInsufficientException : System.Exception
    {
    }
}