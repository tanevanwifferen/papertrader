using System.ComponentModel.DataAnnotations;

namespace PaperTrader.Domain
{
    public interface IOrderInput
    {
        [Required]
        int SessionId { get; }

        [Required]
        decimal Price { get; }

        [Required]
        int Contracts { get; }
    }
}