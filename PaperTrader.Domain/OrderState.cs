namespace PaperTrader.Domain
{
    public enum OrderState
    {
        AcceptedForBidding,
        PendingNew,
        Rejected,
        New,
        Replaced,
        PartiallyFilled,
        Expired,
        Canceled,
        Suspended,
        Stopped,
        Filled,
        Calculated,
        DoneForDay,
        PendingReplace,
        PendingCancel
    }

    public static class OrderStateParser{
        public static string FromOrderState(OrderState state)
        {
            switch(state){
                default:
                    throw new System.NotImplementedException("can't convert " + state);
            }
        }

        public static OrderState FromString(string state)
        {
            switch (state)
            {
                default:
                    throw new System.NotImplementedException("can't convert " + state);
            }
        }
    }
}