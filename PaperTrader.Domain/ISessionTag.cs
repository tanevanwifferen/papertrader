namespace PaperTrader.Domain
{
    public interface ISessionTag{
        int Id{get;}
        int TagId{get;}
        int SessionId{get;}
    }
}