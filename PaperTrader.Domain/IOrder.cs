using System;
using System.Threading.Tasks;

namespace PaperTrader.Domain
{
    /// <summary>
    /// IOrder is a type that defines the buying of selling of an amount of contracts.
    /// </summary>
    public interface IOrder
    {
         /// <summary>
         /// Gets the unique identifier for this order
         /// </summary>
        int Id{ get; }

         /// <summary>
         /// Gets the session id belonging to this order.
         /// </summary>
        int SessionId{get;}

         /// <summary>
         /// Gets the session belonging to this IOrder
         /// </summary>
        ISession Session{get;}

         /// <summary>
         /// Gets the price this order was executed on.
         /// </summary>
        decimal Price{get;}

         /// <summary>
         /// Gets the price at time of the signal
         /// </summary>
        decimal SignalPrice{get;}

         /// <summary>
         /// Gets the amount of contracts
         /// </summary>
        int Contracts{get;}

         /// <summary>
         /// Gets a value indicating the amount ofthe order that has been filled.
         /// </summary>
        int ContractsFilled{get;}

        /// <summary>
        /// Gets the order state for this order.
        /// </summary>
        OrderState OrderState {get;}

         /// <summary>
         /// Gets the time this order was executed.
         /// </summary>
        DateTime ExecutionTime { get; }

         /// <summary>
         /// Gets the time this order was submitted.
         /// </summary>
        DateTime SubmissionTime{get;}

         /// <summary>
         /// Fills a part of the order at a specific time.
         /// </summary>
         /// <param name="executionTime">Time this fill has happened</param>
         /// <param name="amount">Amount to fill</param>
         /// <param name="price">Price to fill at</param>
        Task Fill(DateTime executionTime, int amount, decimal price);
    }
}