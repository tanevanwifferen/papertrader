namespace PaperTrader.Domain
{
     /// <summary>
     /// A SessionType indicates the logic that a session should follor.
     /// </summary>
    public enum SessionType
    {
        /// <summary>
        /// Backtest disables price validation, allows 
        /// any price to be set on an order, and any date.
        /// </summary> 
        Backtest,

        /// <summary>
        /// PaperTrading will fetch prices from BitMEX 
        /// automatically, and will handle order filling 
        /// based upon current price movement
        /// </summary>
        PaperTrading,

        /// <summary>
        /// LiveTrading will not emulate anything, but use 
        /// BitMEX execution data and api connection.
        /// </summary>
        LiveTrading
    }
}