using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PaperTrader.Domain;

namespace PaperTrader.Domain
{
    public interface ISessionInput
    {
        int Id { get; set; }

        [Required]
        SessionType SessionType { get; }

        [Required]
        decimal StartBase { get; }

        string HumanReadableName { get; }

        List<string> Tags { get; }
        int Leverage { get; }
    }
}