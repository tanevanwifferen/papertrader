using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaperTrader.Domain
{
    public interface ITag
    {
        int Id { get; }
        string Value { get; }
        Task<ICollection<ISession>> GetSessions();
    }
}