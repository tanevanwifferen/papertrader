# PaperTrader

paper trader for BitMEX

This app should be an interface between any robot, and the BitMEX exchange. It should handle three use cases: 

1) backtesting
2) paper trading
3) live trading

First backtesting and paper trading will be implemented, then I will move to live trading.

Branch management is done using [https://spin.atomicobject.com/2018/10/10/git-branching-strategy/](branch gardening), which is the strategy that fits my preferences most.